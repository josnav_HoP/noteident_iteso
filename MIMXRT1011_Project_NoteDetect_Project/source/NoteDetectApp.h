/*
 * NoteDetectApp.h
 *
 *  Created on: Mar 24, 2022
 *      Author: josel
 */

#ifndef NOTEDETECTAPP_H_
#define NOTEDETECTAPP_H_

#include "fsl_wm8960.h"
#include "pin_mux.h"
#include "board.h"
#include "clock_config.h"
#include "fsl_codec_common.h"
#include "fsl_codec_adapter.h"
#include "FreeRTOS.h"
#include "NoteDetectApp_Cfg.h"



#define INIT_THREAD_STACKSIZE 128

#define OVER_SAMPLE_RATE (384U)
#define BUFFER_SIZE (1024)
#define BUFFER_NUM (2)
#if defined BOARD_HAS_SDCARD && (BOARD_HAS_SDCARD != 0)
#define DEMO_SDCARD (1U)
#endif
/* demo audio sample rate */
#define DEMO_AUDIO_SAMPLE_RATE (kSAI_SampleRate8KHz)
/* demo audio master clock */
#if (defined FSL_FEATURE_SAI_HAS_MCLKDIV_REGISTER && FSL_FEATURE_SAI_HAS_MCLKDIV_REGISTER) || \
    (defined FSL_FEATURE_PCC_HAS_SAI_DIVIDER && FSL_FEATURE_PCC_HAS_SAI_DIVIDER)
#define DEMO_AUDIO_MASTER_CLOCK OVER_SAMPLE_RATE *DEMO_AUDIO_SAMPLE_RATE
#else
#define DEMO_AUDIO_MASTER_CLOCK DEMO_SAI_CLK_FREQ
#endif
/* demo audio data channel */
#define DEMO_AUDIO_DATA_CHANNEL (2U)
/* demo audio bit width */
#define DEMO_AUDIO_BIT_WIDTH (kSAI_WordWidth16bits)


void AppInitTask(void *pvParameters);

#endif /* NOTEDETECTAPP_H_ */

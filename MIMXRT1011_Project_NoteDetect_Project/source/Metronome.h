/*
 * Metronome.h
 *
 *  Created on: 23 abr 2022
 *      Author: Mercado
 */

#ifndef METRONOME_H_
#define METRONOME_H_

#include <stdint.h>

 //agregar un enum de compases
enum  compaces
{
	a_2_4=2,
	a_3_4=3,
	a_4_4=4

};
//el tempo está definido por un múltiplo de 64 (64 milisegundas es lo que tarda el buffer en llenarse, los números del enum se refieren a milisegundas, haciendo un equivalente de BPM's a milisegundos)
enum tempo
{
	grave			=3008, //aprox 20 bpm  --> quiere decir que una negra suena cada 3008 ms, en este caso una semicorchea dura  188 ms
	lento			=1472, //Aprox 40bpm  	una semicorchea dura 92 ms
	largo 			=1344, //aprox 45 bpm  	una semicorchea dura 84 ms
	larghetto		=1216, //Aprox 50 bpm 	una semicorchea dura 76 ms
	adagio 			=1088, //aprox 55 bpm 	una semicorchea dura 68 ms
	adagietto		=896,  //aprox 67 bpm
	andante 		=832,  //aprox 72 bpm
	moderato 		=704,  //aprox 85 bpm
	allegro 		=512,  //aprox 117 bpm
	vivace 			=448,  //aprox 134 bpm
	prestissimo 	=320,  //aprox 188 bpm
};


//Function Prototypes

void set_metronome(uint16_t bpm,uint8_t compas);
uint8_t metronome_handler();


#endif /* METRONOME_H_ */

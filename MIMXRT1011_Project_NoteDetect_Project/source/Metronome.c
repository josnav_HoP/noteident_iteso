/*
 * Metronome.c
 *
 *  Created on: 23 abr 2022
 *      Author: Ixchel Mercado
 */



#include "Metronome.h"
#include "pin_mux.h"
#include "board.h"


#define EXAMPLE_LED_GPIO     BOARD_USER_LED_GPIO
#define EXAMPLE_LED_GPIO_PIN BOARD_USER_LED_GPIO_PIN


//variables
uint16_t bip_counter=0;
uint8_t bip_compas_counter=0;
uint16_t counter_set_note;
uint8_t counter_set_compas;
uint8_t negra_flag;

struct memtrnm_struct_info
{
	uint16_t bpm;
	uint8_t compas;

}memtrnm_set_info;


void beep ()
{
	GPIO_PinWrite(EXAMPLE_LED_GPIO, EXAMPLE_LED_GPIO_PIN, 1U);
	GPIO_PinWrite(EXAMPLE_LED_GPIO, EXAMPLE_LED_GPIO_PIN, 0U);
}

void compas_beep()
{
	GPIO_PinWrite(EXAMPLE_LED_GPIO, EXAMPLE_LED_GPIO_PIN, 1U);
	GPIO_PinWrite(EXAMPLE_LED_GPIO, EXAMPLE_LED_GPIO_PIN, 0U);
	GPIO_PinWrite(EXAMPLE_LED_GPIO, EXAMPLE_LED_GPIO_PIN, 1U);
	GPIO_PinWrite(EXAMPLE_LED_GPIO, EXAMPLE_LED_GPIO_PIN, 0U);
}


void set_metronome(uint16_t bpm,uint8_t compas)
{
	memtrnm_set_info.bpm = bpm;
	memtrnm_set_info.compas = compas;

	counter_set_note= memtrnm_set_info.bpm/64;

	bip_counter=0;
	bip_compas_counter=0;

	BOARD_InitBootPins();

}




uint8_t metronome_handler()
{
	if(bip_counter<counter_set_note) //si el contaddor se llena es porque ya pasó el tiempo de una negra
		{
			bip_counter++;
			negra_flag=0;
		}
		else
		{
			beep ();
			bip_counter = 0;
			negra_flag=1;

			if(bip_compas_counter<memtrnm_set_info.compas)
			{
				bip_compas_counter++;
			}
			else
			{
				compas_beep();
				bip_compas_counter=0;
			}

		}

	return negra_flag;
}




/*
 * Fourier.h
 *
 *  Created on: Mar 27, 2022
 *      Author: josel
 */

#ifndef FOURIER_H_
#define FOURIER_H_

#include "stdint.h"
#include "arm_math.h"

float32_t do_fft(uint32_t sampleRate, uint32_t bitWidth, uint8_t *buffer, float32_t *fftData, float32_t *fftResult,uint16_t BufferSize);


#endif /* FOURIER_H_ */

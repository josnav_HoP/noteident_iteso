/*
 * NoteDetectApp.c
 *
 *  Created on: Mar 24, 2022
 *      Author: josel
 */


#include "NoteDetectApp.h"

#include "fsl_dmamux.h"
#include "fsl_sai_edma.h"
#include "arm_math.h"
#include "fsl_codec_common.h"
#include "fsl_debug_console.h"

#include "FreeRTOS.h"
#include "task.h"
#include "event_groups.h"

#include "Fourier.h"
#include "Metronome.h"

#define TRANSFER_STARTED_EVENT	(0x01U<<0U)
#define TRANSFER_FINISHED_EVENT (0x01U<<1U)
#define NOTE_IDENTIFIED_EVENT	(0x01U<<2U)
#define DEMO_FINISHED_EVENT (0x01U<<3U)

#define NOISE_OUT_MAGIC_NUMBER (60000.0)

volatile void txCallback(I2S_Type *base, sai_edma_handle_t *handle, status_t status, void *userData);
volatile void rxCallback(I2S_Type *base, sai_edma_handle_t *handle, status_t status, void *userData);


typedef struct
{
	const char * NoteString;
	float32_t NoteFrequency;
}Note_t;

const Note_t NoteTable[] = {
		{"Mi4",330},
		{"Fa4",350},
		{"Fa#4",370},
		{"Sol4",392},
		{"Sol#4",415},
		{"La4",440},
		{"La#4",466},
		{"Do5",523},
		{"Do#5",554},
		{"Re5",587},
		{"Re#5",622},
		{"Mi5",659},
		{"Fa5",698},
		{"Fa#5",739},
		{"Sol5",783},
		{"Sol#5",830},
		{"La#5",880},
		{"La#5",932},
		{"Si5",988},
		{"Do6",1046},
		{"Do#6",1109},
		{"Re6",1175},
		{"Re#6",1245}
};


sai_transfer_t xfer    = {0};
uint8_t rxindex = 0;
float32_t fft_ComplexBuffer[BUFFER_SIZE];
float32_t fft_ResultsBuffer[BUFFER_SIZE/2];

wm8960_config_t wm8960Config = {
    .i2cConfig = {.codecI2CInstance = BOARD_CODEC_I2C_INSTANCE, .codecI2CSourceClock = BOARD_CODEC_I2C_CLOCK_FREQ},
    .route     = kWM8960_RoutePlaybackandRecord,
    .rightInputSource = kWM8960_InputDifferentialMicInput2,
    .playSource       = kWM8960_PlaySourceDAC,
    .slaveAddress     = WM8960_I2C_ADDR,
    .bus              = kWM8960_BusI2S,
    .format = {.mclk_HZ = 6144000U, .sampleRate = kWM8960_AudioSampleRate8KHz, .bitWidth = kWM8960_AudioBitWidth16bit},
    .master_slave = false,
};
codec_config_t boardCodecConfig = {.codecDevType = kCODEC_WM8960, .codecDevConfig = &wm8960Config};
/*
 * AUDIO PLL setting: Frequency = Fref * (DIV_SELECT + NUM / DENOM)
 *                              = 24 * (32 + 77/100)
 *                              = 786.48 MHz
 */

AT_NONCACHEABLE_SECTION_INIT(sai_edma_handle_t txHandle) = {0};
edma_handle_t dmaTxHandle                                = {0};
AT_NONCACHEABLE_SECTION_INIT(sai_edma_handle_t rxHandle) = {0};
edma_handle_t dmaRxHandle                                = {0};


AT_NONCACHEABLE_SECTION_ALIGN(uint8_t audioBuff[BUFFER_SIZE * BUFFER_NUM], 4);

extern codec_config_t boardCodecConfig;
volatile bool istxFinished     = false;
volatile bool isrxFinished     = false;
volatile uint32_t beginCount   = 0;
volatile uint32_t sendCount    = 0;
volatile uint32_t receiveCount = 0;
volatile bool sdcard           = false;
volatile uint32_t fullBlock    = 0;
volatile uint32_t emptyBlock   = BUFFER_NUM;
volatile uint8_t IsReadyForFft = 0;
volatile uint8_t ReceiveReady = 1;

sai_transceiver_t config;
codec_handle_t codecHandle;

EventGroupHandle_t TransferEventHandler;

/**
 *
 * RTOS tasks prototypes
 *
 * */
void ConsoleAppTask(void *pvParameters);
void StartTransferTask(void *pvParameters);
void FindNotes(void *pvParameters);


/*Tasks definitions*/



void AppInitTask(void *pvParameters)
{
    /* Create EDMA handle */
    /*
     * dmaConfig.enableRoundRobinArbitration = false;
     * dmaConfig.enableHaltOnError = true;
     * dmaConfig.enableContinuousLinkMode = false;
     * dmaConfig.enableDebugMode = false;
     */
	edma_config_t dmaConfig = {0};
	uint32_t result;
	status_t StatusCodec;

    EDMA_GetDefaultConfig(&dmaConfig);
    EDMA_Init(EXAMPLE_DMA, &dmaConfig);
    EDMA_CreateHandle(&dmaTxHandle, EXAMPLE_DMA, EXAMPLE_TX_CHANNEL);
    EDMA_CreateHandle(&dmaRxHandle, EXAMPLE_DMA, EXAMPLE_RX_CHANNEL);

    DMAMUX_Init(EXAMPLE_DMAMUX);
    DMAMUX_SetSource(EXAMPLE_DMAMUX, EXAMPLE_TX_CHANNEL, (uint8_t)EXAMPLE_SAI_TX_SOURCE);
    DMAMUX_EnableChannel(EXAMPLE_DMAMUX, EXAMPLE_TX_CHANNEL);
    DMAMUX_SetSource(EXAMPLE_DMAMUX, EXAMPLE_RX_CHANNEL, (uint8_t)EXAMPLE_SAI_RX_SOURCE);
    DMAMUX_EnableChannel(EXAMPLE_DMAMUX, EXAMPLE_RX_CHANNEL);

    /* SAI init */
    SAI_Init(DEMO_SAI);

    SAI_TransferTxCreateHandleEDMA(DEMO_SAI, &txHandle, txCallback, NULL, &dmaTxHandle);
    SAI_TransferRxCreateHandleEDMA(DEMO_SAI, &rxHandle, rxCallback, NULL, &dmaRxHandle);

    /* I2S mode configurations */
    SAI_GetClassicI2SConfig(&config, DEMO_AUDIO_BIT_WIDTH, kSAI_MonoRight, kSAI_Channel0Mask);
    SAI_TransferTxSetConfigEDMA(DEMO_SAI, &txHandle, &config);
    config.syncMode = kSAI_ModeSync;
    SAI_TransferRxSetConfigEDMA(DEMO_SAI, &rxHandle, &config);

    /* set bit clock divider */
    SAI_TxSetBitClockRate(DEMO_SAI, DEMO_AUDIO_MASTER_CLOCK, DEMO_AUDIO_SAMPLE_RATE, DEMO_AUDIO_BIT_WIDTH,
                          DEMO_AUDIO_DATA_CHANNEL);
    SAI_RxSetBitClockRate(DEMO_SAI, DEMO_AUDIO_MASTER_CLOCK, DEMO_AUDIO_SAMPLE_RATE, DEMO_AUDIO_BIT_WIDTH,
                          DEMO_AUDIO_DATA_CHANNEL);

    result = NVIC_GetPriority(DMA1_IRQn);
    NVIC_SetPriority(DMA1_IRQn,3);
    NVIC_SetPriority(SAI1_IRQn,3);
/* master clock configurations */
#if (defined(FSL_FEATURE_SAI_HAS_MCR) && (FSL_FEATURE_SAI_HAS_MCR)) || \
    (defined(FSL_FEATURE_SAI_HAS_MCLKDIV_REGISTER) && (FSL_FEATURE_SAI_HAS_MCLKDIV_REGISTER))
#if defined(FSL_FEATURE_SAI_HAS_MCLKDIV_REGISTER) && (FSL_FEATURE_SAI_HAS_MCLKDIV_REGISTER)
    mclkConfig.mclkHz          = DEMO_AUDIO_MASTER_CLOCK;
    mclkConfig.mclkSourceClkHz = DEMO_SAI_CLK_FREQ;
#endif
    SAI_SetMasterClockConfig(DEMO_SAI, &mclkConfig);
#endif

    /* Use default setting to init codec */
    StatusCodec = CODEC_Init(&codecHandle, &boardCodecConfig);

    /* Enable interrupt to handle FIFO error */
    SAI_TxEnableInterrupts(DEMO_SAI, kSAI_FIFOErrorInterruptEnable);
    SAI_RxEnableInterrupts(DEMO_SAI, kSAI_FIFOErrorInterruptEnable);
    EnableIRQ(DEMO_SAI_TX_IRQ);
    EnableIRQ(DEMO_SAI_RX_IRQ);


    if (xTaskCreate(ConsoleAppTask,"ConsoleApp", INIT_THREAD_STACKSIZE ,NULL , configMAX_PRIORITIES-1,NULL) != pdPASS)
    {

    }

    TransferEventHandler = xEventGroupCreate();


    vTaskDelete(NULL);
}


void ConsoleAppTask(void *pvParameters)
{
	uint8_t ConsoleInput_u8;

	if (xTaskCreate(StartTransferTask,"TransferTask", INIT_THREAD_STACKSIZE*1 ,NULL , configMAX_PRIORITIES-2,NULL) != pdPASS)
	{
		PRINTF("Task Creation Failed");
	}
	if (xTaskCreate(FindNotes,"NoteIdentificationTask", (INIT_THREAD_STACKSIZE*4) ,NULL , configMAX_PRIORITIES-3,NULL) != pdPASS)
	{
		PRINTF("Task Creation Failed");
	}

	while(1)
	{
		PRINTF("Press 1 to start the demo!\r\n");
		ConsoleInput_u8 = GETCHAR();
        PUTCHAR(ConsoleInput_u8);
        if('1'==ConsoleInput_u8)
        {
        	PRINTF("5 seconds of demo started\r\n");
            xEventGroupWaitBits(TransferEventHandler, DEMO_FINISHED_EVENT, true, false, portMAX_DELAY);
        }
	}
}


void StartTransferTask(void *pvParameters)
{



    /* Reset SAI internal logic */
    SAI_TxSoftwareReset(SAI1, kSAI_ResetTypeSoftware);
    SAI_RxSoftwareReset(SAI1, kSAI_ResetTypeSoftware);

    xfer.dataSize = BUFFER_SIZE;

	while(1)
	{
		xfer.data = audioBuff + (rxindex * BUFFER_SIZE);
		if (SAI_TransferReceiveEDMA(SAI1, &rxHandle, &xfer) == kStatus_Success)
		{
			rxindex = ((rxindex + 1)&0x01);
			xEventGroupWaitBits(TransferEventHandler,TRANSFER_FINISHED_EVENT , true, true, portMAX_DELAY);
			xEventGroupSetBits(TransferEventHandler,TRANSFER_STARTED_EVENT);
		}
	}
}


void FindNotes(void *pvParameters)
{


	float32_t HzNote;
	uint16_t InitialIndex_FreqFinder = 330/(DEMO_AUDIO_SAMPLE_RATE/(BUFFER_SIZE/2));//Do4 Freq is 261
	uint16_t FinalIndex_FreqFinder = 1318/(DEMO_AUDIO_SAMPLE_RATE/(BUFFER_SIZE/2));//Si5 Freq is 987
	float32_t HzPerBin = (DEMO_AUDIO_SAMPLE_RATE/(BUFFER_SIZE/2));
	const char *ChordArray[MAX_NOTES_PER_CHORD];
	uint8_t ChordIndex = 0;
	uint8_t NoteTableIndex = 0;
	float32_t NotesMidPoint = 0;

	while(1)
	{
		xEventGroupWaitBits(TransferEventHandler,TRANSFER_STARTED_EVENT , true, true, portMAX_DELAY);
		ChordIndex = 0;
		InitialIndex_FreqFinder = 330/(DEMO_AUDIO_SAMPLE_RATE/(BUFFER_SIZE/2));//Do4 Freq is 261
		memset(ChordArray,0,MAX_NOTES_PER_CHORD*4);
		HzNote = do_fft(DEMO_AUDIO_SAMPLE_RATE,16,audioBuff+(rxindex*BUFFER_SIZE),fft_ComplexBuffer,fft_ResultsBuffer,BUFFER_SIZE);
		/*Iterate over desired section of FFT results*/
		do{
			if(fft_ResultsBuffer[InitialIndex_FreqFinder]>NOISE_OUT_MAGIC_NUMBER)
			{
				if((fft_ResultsBuffer[InitialIndex_FreqFinder]>fft_ResultsBuffer[InitialIndex_FreqFinder+1]) &&
				(fft_ResultsBuffer[InitialIndex_FreqFinder] > fft_ResultsBuffer[InitialIndex_FreqFinder-1]))
				{
					HzNote = InitialIndex_FreqFinder * HzPerBin;
					NoteTableIndex = 0;
					while(NoteTableIndex < (sizeof(NoteTable)/sizeof(NoteTable[0])))
					{
						NotesMidPoint = (NoteTable[NoteTableIndex+1].NoteFrequency - NoteTable[NoteTableIndex].NoteFrequency)/2;
						if(HzNote<NoteTable[NoteTableIndex].NoteFrequency &&
								(HzNote<(NoteTable[NoteTableIndex+1].NoteFrequency)+NotesMidPoint))
						{
							ChordArray[ChordIndex] = NoteTable[NoteTableIndex].NoteString;
							break;
						}
						NoteTableIndex++;
					}
//					ChordArray[ChordIndex] = InitialIndex_FreqFinder * HzPerBin;
					ChordIndex++;
				}
			}
			InitialIndex_FreqFinder++;
		}while((InitialIndex_FreqFinder <= FinalIndex_FreqFinder) && (ChordIndex < MAX_NOTES_PER_CHORD));



		PRINTF("%s,",(uint32_t)ChordArray[0]);
		PRINTF("%s,",(uint32_t)ChordArray[1]);
		PRINTF("%s\r\n",(uint32_t)ChordArray[2]);
	}

}

volatile void txCallback(I2S_Type *base, sai_edma_handle_t *handle, status_t status, void *userData)
{
    sendCount++;
    emptyBlock++;

    if (sendCount == beginCount)
    {
        istxFinished = true;
        SAI_TransferTerminateSendEDMA(base, handle);
        sendCount = 0;
    }
}

volatile void rxCallback(I2S_Type *base, sai_edma_handle_t *handle, status_t status, void *userData)
{

	BaseType_t xHigherPriorityTaskWoken, xResult;

    receiveCount++;
    fullBlock++;
    IsReadyForFft = 1;
    xEventGroupSetBitsFromISR(TransferEventHandler,TRANSFER_FINISHED_EVENT,&xHigherPriorityTaskWoken);
    ReceiveReady = 1;

    portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
//    if (receiveCount == beginCount)
//    {
//        isrxFinished = true;
//        SAI_TransferTerminateReceiveEDMA(base, handle);
//        receiveCount = 0;
//    }
}


################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../amazon-freertos/freertos/event_groups.c \
../amazon-freertos/freertos/list.c \
../amazon-freertos/freertos/queue.c \
../amazon-freertos/freertos/stream_buffer.c \
../amazon-freertos/freertos/tasks.c \
../amazon-freertos/freertos/timers.c 

OBJS += \
./amazon-freertos/freertos/event_groups.o \
./amazon-freertos/freertos/list.o \
./amazon-freertos/freertos/queue.o \
./amazon-freertos/freertos/stream_buffer.o \
./amazon-freertos/freertos/tasks.o \
./amazon-freertos/freertos/timers.o 

C_DEPS += \
./amazon-freertos/freertos/event_groups.d \
./amazon-freertos/freertos/list.d \
./amazon-freertos/freertos/queue.d \
./amazon-freertos/freertos/stream_buffer.d \
./amazon-freertos/freertos/tasks.d \
./amazon-freertos/freertos/timers.d 


# Each subdirectory must supply rules for building sources it contributes
amazon-freertos/freertos/%.o: ../amazon-freertos/freertos/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DCPU_MIMXRT1011DAE5A -DCPU_MIMXRT1011DAE5A_cm7 -DFSL_RTOS_BM -DSDK_OS_BAREMETAL -DSDK_DEBUGCONSOLE=1 -DCR_INTEGER_PRINTF -DPRINTF_FLOAT_ENABLE=0 -DFSL_RTOS_FREE_RTOS -DSDK_OS_FREE_RTOS -DXIP_EXTERNAL_FLASH=1 -DXIP_BOOT_HEADER_ENABLE=1 -D__MCUXPRESSO -D__USE_CMSIS -DDEBUG -I"C:\Users\josel\Documents\MCUXpressoIDE_11.4.0_6237\workspace\MIMXRT1011_Project_NoteDetect_Project\drivers" -I"C:\Users\josel\Documents\MCUXpressoIDE_11.4.0_6237\workspace\MIMXRT1011_Project_NoteDetect_Project\xip" -I"C:\Users\josel\Documents\MCUXpressoIDE_11.4.0_6237\workspace\MIMXRT1011_Project_NoteDetect_Project\codec" -I"C:\Users\josel\Documents\MCUXpressoIDE_11.4.0_6237\workspace\MIMXRT1011_Project_NoteDetect_Project\device" -I"C:\Users\josel\Documents\MCUXpressoIDE_11.4.0_6237\workspace\MIMXRT1011_Project_NoteDetect_Project\amazon-freertos\freertos\portable" -I"C:\Users\josel\Documents\MCUXpressoIDE_11.4.0_6237\workspace\MIMXRT1011_Project_NoteDetect_Project\amazon-freertos\include" -I"C:\Users\josel\Documents\MCUXpressoIDE_11.4.0_6237\workspace\MIMXRT1011_Project_NoteDetect_Project\source" -I"C:\Users\josel\Documents\MCUXpressoIDE_11.4.0_6237\workspace\MIMXRT1011_Project_NoteDetect_Project\component\lists" -I"C:\Users\josel\Documents\MCUXpressoIDE_11.4.0_6237\workspace\MIMXRT1011_Project_NoteDetect_Project\component\uart" -I"C:\Users\josel\Documents\MCUXpressoIDE_11.4.0_6237\workspace\MIMXRT1011_Project_NoteDetect_Project\component\i2c" -I"C:\Users\josel\Documents\MCUXpressoIDE_11.4.0_6237\workspace\MIMXRT1011_Project_NoteDetect_Project\CMSIS" -I"C:\Users\josel\Documents\MCUXpressoIDE_11.4.0_6237\workspace\MIMXRT1011_Project_NoteDetect_Project\component\serial_manager" -I"C:\Users\josel\Documents\MCUXpressoIDE_11.4.0_6237\workspace\MIMXRT1011_Project_NoteDetect_Project\utilities" -I"C:\Users\josel\Documents\MCUXpressoIDE_11.4.0_6237\workspace\MIMXRT1011_Project_NoteDetect_Project\component\lists" -I"C:\Users\josel\Documents\MCUXpressoIDE_11.4.0_6237\workspace\MIMXRT1011_Project_NoteDetect_Project\component\i2c" -I"C:\Users\josel\Documents\MCUXpressoIDE_11.4.0_6237\workspace\MIMXRT1011_Project_NoteDetect_Project\codec" -I"C:\Users\josel\Documents\MCUXpressoIDE_11.4.0_6237\workspace\MIMXRT1011_Project_NoteDetect_Project\drivers" -I"C:\Users\josel\Documents\MCUXpressoIDE_11.4.0_6237\workspace\MIMXRT1011_Project_NoteDetect_Project\device" -I"C:\Users\josel\Documents\MCUXpressoIDE_11.4.0_6237\workspace\MIMXRT1011_Project_NoteDetect_Project\xip" -I"C:\Users\josel\Documents\MCUXpressoIDE_11.4.0_6237\workspace\MIMXRT1011_Project_NoteDetect_Project\CMSIS" -I"C:\Users\josel\Documents\MCUXpressoIDE_11.4.0_6237\workspace\MIMXRT1011_Project_NoteDetect_Project\component\serial_manager" -I"C:\Users\josel\Documents\MCUXpressoIDE_11.4.0_6237\workspace\MIMXRT1011_Project_NoteDetect_Project\source" -I"C:\Users\josel\Documents\MCUXpressoIDE_11.4.0_6237\workspace\MIMXRT1011_Project_NoteDetect_Project\utilities" -I"C:\Users\josel\Documents\MCUXpressoIDE_11.4.0_6237\workspace\MIMXRT1011_Project_NoteDetect_Project\component\uart" -I"C:\Users\josel\Documents\MCUXpressoIDE_11.4.0_6237\workspace\MIMXRT1011_Project_NoteDetect_Project\amazon-freertos\freertos\portable" -I"C:\Users\josel\Documents\MCUXpressoIDE_11.4.0_6237\workspace\MIMXRT1011_Project_NoteDetect_Project\amazon-freertos\include" -I"C:\Users\josel\Documents\MCUXpressoIDE_11.4.0_6237\workspace\MIMXRT1011_Project_NoteDetect_Project\board" -I"C:\Users\josel\Documents\MCUXpressoIDE_11.4.0_6237\workspace\MIMXRT1011_Project_NoteDetect_Project" -O0 -fno-common -g3 -Wall -c -ffunction-sections -fdata-sections -ffreestanding -fno-builtin -fmerge-constants -fmacro-prefix-map="../$(@D)/"=. -mcpu=cortex-m7 -mfpu=fpv5-sp-d16 -mfloat-abi=hard -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


